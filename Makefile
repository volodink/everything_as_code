all: clean prepare
	cd lessons/easc && make && cp dist/easc.pdf ../../dist/easc.pdf


clean:
	rm -rf dist

prepare:
	mkdir -p dist

gitpod-bootstrap:
	sudo apt-get update && sudo apt-get dist-upgrade -y
