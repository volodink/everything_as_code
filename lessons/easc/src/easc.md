---
marp: true
theme: lection
paginate: true
# size: 4:3
---

<!-- _class: lead -->
<!-- _paginate: false -->

#

#

#


# Everything() as Code (EaC)

#

## Философия, подходы, технологии

#

#

#

#

#

#

##### Константин Володин

---
<!-- _paginate: false -->

# Немного о себе

## Володин Константин Игоревич

:snake: Python: люблю, применяю, преподаю

:robot::rocket: Разработчик встраиваемых систем

:sloth: Преподаватель САПР Altium Designer

:scroll: Выпускник ПензГТУ (ПГТА) с отличием

:eyes: Moscow State University CanSAT expert

:exploding_head: MTA SQL, MCP, Intel NNGU PCF и т.п.

:ru: Немного преподаю на кафедре ИТС ПензГТУ

---

![bg w:100%](img/qwe.png)

---

# Популярные заблуждения

- "Мне английский не понадобится"
- "Математика (программисту или вообще) не нужна
- "Программировать я не буду по жизни"

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Мне английский не понадобится 

---

<!-- _paginate: false -->

![bg](img/code.jpg)![bg](img/art.jpg)

---

![bg](https://avatars.dzeninfra.ru/get-zen_doc/1645803/pub_614eef6d4e56001d9a644837_6150a6eb7b505c416827fec5/scale_2400)

---

![bg w:200%](img/english.jpg)![bg w:200%](img/chineese.png)

---

![bg w:100%](https://o.quizlet.com/IXpLnvRAjHyixS7QNwU2sw_b.png)

---

# Китай стал лидером по цитируемым научным статьям (данные NISTEP и NATURE INDEX) 

![w:100%](https://habrastorage.org/r/w1560/getpro/habr/upload_files/c12/c7d/ca8/c12c7dca869485bcb15c7c07fa010d4a.jpg)


[https://habr.com/ru/articles/683718](https://habr.com/ru/articles/683718/)

---

<!-- _class: two_columns -->

<div class="columns">
<div class="lcol">

# Python

```
name = input()
print(f"Good evening, {name}")
```

</div>
<div class="rcol">

# C++

```
#include <iostream>
#include <string>
using namespace std;
int main() {
    string name;
    cin >> name;
    cout << "Good evening, " << name << endl;
    return 0;
}
```

</div>
</div>

[https://www.bitdegree.org/tutorials/python-vs-c-plus-plus/](https://www.bitdegree.org/tutorials/python-vs-c-plus-plus/)

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Математика (программисту или вообще) не нужна

---
<!-- _paginate: false -->
![bg](https://boinc.bakerlab.org/rosetta/rah_img/jhr_vs_covid.gif)

---

![bg](https://upload.wikimedia.org/wikipedia/commons/9/92/ModalAnalysis_TrussBridge.gif)

---

![bg h:100%](https://www.researchgate.net/publication/324655788/figure/fig3/AS:617544809472000@1524245564494/2D-composite-of-pillared-graphene-based-on-CNT-9-9-with-a-length-of-24-nm-a-atomic.png)

---

![bg w:100%](https://theoffroading.com/wp-content/uploads/2023/04/Cow-vs.-Jeep-Aerodynamics.jpg?ezimgfmt=ng%3Awebp%2Fngcb1%2Frs%3Adevice%2Frscb1-1)

---

<!-- _paginate: false -->

![bg w:100%](https://www.cnet.com/a/img/resize/31d13700449873f77ab8924249fea8d97fa2ec72/hub/2012/03/22/f42a33d1-fdba-11e2-8c7c-d4ae52e62bcc/Angry_Birds_Space_HD.jpg?auto=webp&fit=crop&height=675&width=1200)

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Программировать я не буду по жизни

---

![bg w:100%](https://www.bkbprecision.com/storage/425/HAAS-VF3-CNC-freesmachine-BKB-Precision.jpg)

---

![bg w:100%](https://www.roboris.it/wp-content/uploads/2022/06/eurekagcode1-1-1.png)

---

![bg w:100%](https://3dtoday.ru/upload/main/02a/2.jpg)

---

![bg w:100%](https://formfutura.com/gifs/high-speed-printing.gif)

---

![bg w:100%](https://i.pinimg.com/originals/e4/f8/90/e4f890268b1f15c64c6996110f170376.gif)

---

![bg w:100%](https://i.pinimg.com/originals/de/fe/63/defe630a60b725507950c9c99acce84a.gif)

---

![bg w:100%](https://labster-image-manager.s3.amazonaws.com/v2/NAP/000bb75b-301f-40ff-bc36-be52ef5fb474/NAP_DNACode.en.x1024.png)

---

![bg h:100%](https://media.springernature.com/full/springer-static/image/art%3A10.1038%2Fnature01410/MediaObjects/41586_2003_Article_BFnature01410_Fig1_HTML.jpg?as=webp)

---

![bg w:100%](https://blog.enterprisedna.co/wp-content/uploads/2023/05/ada7a95d-959e-4c9e-9185-a857c652e46a-1.png)

---

![bg w:100%](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR2rZ5ATnyv2xsKehRz1ahleMuUu1gAGPX8gF_sYMsHMA&s)

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Everything(*) as Code (EaC)

---

# Everything as Code (EaC): Философский взгляд
* __Формализм__
EaC предполагает, что мир, или, по крайней мере, __мир, созданный человеком, может быть описан и изменен__ с помощью формальных языков, таких как код.
* __Детерминизм__
EaC подразумевает детерминистический взгляд, при котором желаемое __состояние системы__ может быть точно определено в коде.
* __Абстракция__
Код действует как слой абстракции, отделяя пользователя от  __underlying complexity__.

---

![bg w:101%](img/cicd.png)

---

![bg w:100%](https://wiiisdom.com/wp-content/uploads/2022/03/rise-ci-cd-analytics-feature-img.png)

---

![bg h:95%](https://wiiisdom.com/wp-content/uploads/2022/03/implementing-ci-cd-semantic-layer.png)

---

# Эволюция концепции

_Доисторическая эпоха (<=2006г.)_

1. Ручная установка 
    - OС 
    - ПО
2. Ручное копирование новых страниц
    - ftp
3. Ручное обновление базы данных
4. Система контроля версий
    - subversion

Боль

![bg right:30%](https://avatars.dzeninfra.ru/get-zen_doc/271828/pub_65ca84304ef96c6de35758e4_65ca84eff30b14621579b6d1/scale_1200)

---

# Эволюция концепции

В 2006 был запущен сервис __AWS Elastic Compute Cloud__. 
Аналог: [Яндекс Облако](https://cloud.yandex.ru/ru/)

_Infrastructure-as-Code; Iac_

Подход для управления и описания инфраструктуры __через конфигурационные файлы__, а не через ручное редактирование конфигураций на серверах или интерактивное взаимодействие.

![bg right:30%](https://cdn.sanity.io/images/hgftikht/production/85790ad24932b3dc0c117cd33eeb8a03b40ca9f2-2154x1279.png?w=1400&h=831&fit=crop)

Получше стало :)

---

# _Infrastructure-as-Code; Iac_

- код проекта
- код тестов проекта

- код конфигурации облачной машины или нескольких машин
- код базы данных

- код скриптов-помощников

---

<!-- _class: big-code -->

# Код веб приложения

```python
import time

import redis
from flask import Flask

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
```

---

<!-- _class: big-code -->

# Конфигурация контейнера (Dockerfile)

```dockerfile
# syntax=docker/dockerfile:1
FROM python:3.10-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
```

---

<!-- _class: big-code -->

# Описание локального окружения (docker compose)

```yaml
services:
  web:
    build: .
    ports:
      - "8000:5000"
  redis:
    image: "redis:alpine"
```

---

<!-- _class: big-code -->

# Запуск локального окружения

```bash
>> docker compose up
```

```bash
>> docker image ls
```

```
REPOSITORY        TAG           IMAGE ID      CREATED        SIZE
composetest_web   latest        e2c21aa48cc1  4 minutes ago  93.8MB
python            3.4-alpine    84e6077c7ab6  7 days ago     82.5MB
redis             alpine        9d8fa9aa0e5b  3 weeks ago    27.5MB
```

---

![bg h:90%](https://docs.docker.com/compose/images/quick-hello-world-2.png)

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Управление инфраструктурой

---

# Terraform

__Terraform__ – это open-source инструмент от компании HashiCorp, который используется для управления инфраструктурой как код (IaC). Он позволяет декларативно описывать желаемое состояние инфраструктуры, а затем автоматически создавать, обновлять и удалять ресурсы, необходимые для достижения этого состояния.

```
resource "aws_instance" "example_server" {
  availability_zone = "${module.my_host.availability_zone}"
  instance_type = "t2.micro" 
  size          = 25    # in GiB
  count         = 10                                 
}
```

```bash
>> terraform apply
```

---

# Ansible

__Ansible__ – это инструмент автоматизации IT с открытым исходным кодом, который позволяет упростить развертывание и обслуживание ваших приложений и систем. 

```
[webservers]       ---                      ansible-playbook install_nginx.yml
192.168.1.10       - hosts: webservers
192.168.1.11         tasks:
                     - name: Install Nginx
                       apt:
                         name: nginx
                         state: present
                     - name: Start Nginx
                       service:
                         name: nginx
                         state: started
                         enabled: yes
```

---

![bg h:100%](img/photo_2022-09-16_15-10-15.jpg)

---

![bg w:95%](https://www.element61.be/sites/default/files/img_competences/Asset-33-1.png)

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->
# <!--fit--> Системы контроля версий (VCS)

---

# Git

__Git__ - это распределенная система контроля версий, которая позволяет отслеживать изменения файлов, работать над проектами совместно с другими людьми и легко откатывать изменения.

- Сохранение истории изменений
    - репозиторий или репа (__repository__), коммит (__commit__), __push__, __pull__
- Разветвление и слияние
    - __branch__, __merge__
- Совместная работа
- Децентрализованная разработка

---

# Платформы для размещения репозиториев с кодом

![bg w:50%](https://github.githubassets.com/assets/GitHub-Mark-ea2971cee799.png)
![bg w:50%](https://static-00.iconduck.com/assets.00/gitlab-icon-2048x1885-1o0cwkbx.png)

---

![bg h:100%](https://github.com/ruhulmus/Git-Flow-Architecture/blob/main/Git-FLow.png?raw=true)

---

<!-- _class: manylines -->

# Everything as Code (EaC)
- код проекта
- код тестов проекта
- код конфигурации контейнера или группы контейнеров
- код конфигурации облачной машины или нескольких машин
- код базы данных (миграции базы данных)
- код скриптов-помощников

- __документация__ .. __диплом?__
- __диаграммы__
- __скрипты автоматизации__
- __др. цифровые исходники в виде текста__: 3д модели и т.п.

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Документация как код

---

<!-- _class: manylines -->

# LaTEX

__LaTeX__ (произносится как "лэйтех" или "латех") –  система компьютерной вёрстки, основанная на языке TeX.

- позволяет создавать документы с профессиональным типографским качеством
- Используется для написания:
    - Научных работ: статей, книг, диссертаций;
    - Технических документов: инструкций, руководств, спецификаций
    - Книг: художественных, учебных, научно-популярных
    - Презентаций: слайдов, докладов, лекций;
    - Резюме: CV, портфолио.

https://latex-project.org/

---

![bg](img/scale_2400.png)

---

# Reveal.js - THE HTML PRESENTATION FRAMEWORK

https://revealjs.com/


<iframe src="https://revealjs.com/#/4"></iframe>

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Диаграммы как код

---

<!-- _class: oneline -->

# PlantUML

```
@startuml
left to right direction
skinparam packageStyle rectangle
actor Заказчик
actor Менеджер
rectangle Заказ {
  Заказчик -- (заказ)
  (заказ) .> (оплата) : включает
  (консультация) .> (заказ) : дополняет
  (заказ) -- Менеджер
}
@enduml
```

https://plantuml.com/

![bg right w:100%](img/usecase.png)

---

# Diagrams

```python
with Diagram("Advanced Web Service with On-Premise", show=False):
    ingress = Nginx("ingress")

    metrics = Prometheus("metric")
    metrics << Grafana("monitoring")

    with Cluster("Service Cluster"):
        grpcsvc = [
            Server("grpc1"),
            Server("grpc2"),
            Server("grpc3")]

    with Cluster("Sessions HA"):
        primary = Redis("session")
        primary - Redis("replica") << metrics
        grpcsvc >> primary

    with Cluster("Database HA"):
        primary = PostgreSQL("users")
        primary - PostgreSQL("replica") << metrics
        grpcsvc >> primary

    aggregator = Fluentd("logging")
    aggregator >> Kafka("stream") >> Spark("analytics")

    ingress >> grpcsvc >> aggregator
```

![bg right:50% w:120%](https://diagrams.mingrammer.com/img/advanced_web_service_with_on-premise.png)

---

<!-- _class: oneline-invert -->
<!-- _paginate: false -->

# <!--fit--> Автоматизация действий пользователя 

---

# Selenium

```
options = webdriver.FirefoxOptions()
options.add_argument('--headless')
options.add_argument('--window-size=1920x1080')

driver = webdriver.Firefox(options=options)

driver.get("https://edu.penzgtu.ru/login/index.php")
time.sleep(1)

driver.maximize_window()
time.sleep(1)

login = base64.b64decode(os.environ['K8S_SECRET_MOODLE_LOGIN']).decode("utf-8")
driver.find_element_by_id('username').send_keys(login)
time.sleep(1)

password = base64.b64decode(os.environ['K8S_SECRET_MOODLE_PASSWORD']).decode("utf-8")
driver.find_element_by_id('password').send_keys(password)
time.sleep(1)

driver.find_element_by_id('loginbtn').click()
time.sleep(2)

driver.get('https://edu.penzgtu.ru/mod/assign/view.php?id=89048&action=editsubmission')
time.sleep(5)

simple_file_upload(driver, args.report_file_path)
driver.close()
```

![bg right:20% h:30%](https://camo.githubusercontent.com/8fd05825a2b55ee599b37e1695a4bbe2d16d8e43ace3a6ba5a9fe7929f2d153c/68747470733a2f2f73656c656e69756d2e6465762f696d616765732f73656c656e69756d5f6c6f676f5f7371756172655f677265656e2e706e67)

---

# Советы

1. Изучить основы Git
2. Завести аккаунт на 
- [gitlab.com](https://gitlab.com/)
- [github.com](https://github.com/)
3. Хранить код лабораторных, курсовых и диплома в репозиториях
4. Разрабатывать пет-проекты и прикладывайте ссылки на них в работы и резюме
5. Переводите на код все что можете :)

---

# Эта презентация тоже код :scream:

![bg h:70%](https://marp.app/og-images/marp-for-vs-code-v1.jpg)

---

<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!
![bg right:25% w:105%](img/qr-code.svg)

https://shorturl.at/afDP5


tg: [@volodink]( @volodink )
email: volodin.konstantin@gmail.com

---

# Ссылки

1. https://techbeacon.com/enterprise-it/6-lessons-everything-code-shift
2. https://venturebeat.com/automation/what-everything-as-code-is-and-why-it-matters/
3. https://www.cloudbees.com/blog/what-is-everything-as-code-eac
4. https://www.adaptavist.com/blog/everything-as-code-embracing-the-codified-evolution
5. https://openpracticelibrary.com/practice/everything-as-code/
6. https://www.hpcwire.com/2020/03/24/rosettahome-rallies-a-legion-of-computers-against-the-coronavirus/
7. https://www.youtube.com/watch?v=ODEIN5V3yLg

---

# Ссылки

8. https://3dtoday.ru/blogs/artms/not-dieninny-openscad
9. https://www.nature.com/articles/nature01410
10. https://github.com/
11. https://gitlab.com/
12. https://marp.app/
13. https://cloud.ru/ru/blog/cicd-about
14. https://diagrams.mingrammer.com/
15. https://spacelift.io/blog/terraform-ec2-instance
16. https://www.selenium.dev/